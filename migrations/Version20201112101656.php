<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201112101656 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE panier (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, user_id INTEGER NOT NULL, produit_id INTEGER DEFAULT NULL, date_achat DATE NOT NULL, quantite INTEGER NOT NULL)');
        $this->addSql('CREATE INDEX IDX_24CC0DF2A76ED395 ON panier (user_id)');
        $this->addSql('CREATE INDEX IDX_24CC0DF2F347EFB ON panier (produit_id)');
        $this->addSql('DROP INDEX IDX_29A5EC271237A8DE');
        $this->addSql('CREATE TEMPORARY TABLE __temp__produit AS SELECT id, type_produit_id, nom, prix, date_lancement, photo, stock, disponible FROM produit');
        $this->addSql('DROP TABLE produit');
        $this->addSql('CREATE TABLE produit (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, type_produit_id INTEGER DEFAULT NULL, nom VARCHAR(255) NOT NULL COLLATE BINARY, prix NUMERIC(8, 2) DEFAULT NULL, date_lancement DATE DEFAULT NULL, photo VARCHAR(255) DEFAULT NULL COLLATE BINARY, stock INTEGER DEFAULT NULL, disponible BOOLEAN DEFAULT NULL, CONSTRAINT FK_29A5EC271237A8DE FOREIGN KEY (type_produit_id) REFERENCES type_produit (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO produit (id, type_produit_id, nom, prix, date_lancement, photo, stock, disponible) SELECT id, type_produit_id, nom, prix, date_lancement, photo, stock, disponible FROM __temp__produit');
        $this->addSql('DROP TABLE __temp__produit');
        $this->addSql('CREATE INDEX IDX_29A5EC271237A8DE ON produit (type_produit_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE panier');
        $this->addSql('DROP INDEX IDX_29A5EC271237A8DE');
        $this->addSql('CREATE TEMPORARY TABLE __temp__produit AS SELECT id, type_produit_id, nom, prix, date_lancement, photo, stock, disponible FROM produit');
        $this->addSql('DROP TABLE produit');
        $this->addSql('CREATE TABLE produit (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, type_produit_id INTEGER DEFAULT NULL, nom VARCHAR(255) NOT NULL, prix NUMERIC(8, 2) DEFAULT NULL, date_lancement DATE DEFAULT NULL, photo VARCHAR(255) DEFAULT NULL, stock INTEGER DEFAULT NULL, disponible BOOLEAN DEFAULT NULL)');
        $this->addSql('INSERT INTO produit (id, type_produit_id, nom, prix, date_lancement, photo, stock, disponible) SELECT id, type_produit_id, nom, prix, date_lancement, photo, stock, disponible FROM __temp__produit');
        $this->addSql('DROP TABLE __temp__produit');
        $this->addSql('CREATE INDEX IDX_29A5EC271237A8DE ON produit (type_produit_id)');
    }
}
