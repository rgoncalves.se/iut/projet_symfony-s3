<?php


namespace App\Controller\Client;


use App\Entity\Commande;
use App\Entity\LigneCommande;
use App\Entity\Panier;
use App\Entity\Produit;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

use DateTime;

class CommandeController extends AbstractController
{

    /**
     * @Route("/client/commande/show", name="client_commande_show", methods={"GET", "POST"})
     * @param Request $request
     */
    public function showCommande(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $paniers = $this->getPaniersForUser($user);
        $commandes = $this->getCommandesForUser($user);
        $commandes = $this->getDoctrine()->getRepository(Commande::class)->findAll();
        $lignes = $this->getDoctrine()->getRepository(LigneCommande::class)->findAll();

        return $this->render("client/boutique/commande.twig",
            ["commandes" => $commandes, "paniers" => $paniers, "lignes" => $lignes]);
    }

    /**
     * @Route("/client/commande/validate", name="client_commande_validate", methods={"POST"})
     * @param Request $request
     */
    public function validateCommande(Request $request)
    {
        $manager = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $paniers = $this->getPaniersForUser($user);

        $cmd = new Commande();
        $cmd->setDate(new DateTime("now"));
        $cmd->setUser($user);

        foreach ($paniers as $panier) {
            $ligne = new LigneCommande();
            $ligne->setQuantite($panier->getQuantite());
            $ligne->setProduit($panier->getProduit());
            $ligne->setCommande($cmd);
            $ligne->setPrix($panier->getProduit()->getPrix());

            $manager->persist($ligne);
            $cmd->addLigneCommande($ligne);

            $manager->remove($panier);

            $manager->persist($cmd);
            $manager->flush();

        }
        return $this->redirectToRoute("client_commande_show");
    }

    private function getPaniersForUser($user) {
        $paniers = $this->getDoctrine()->getRepository(Panier::class)->findAll();
        foreach ($paniers as $panier) {
            if ($panier->getUser()->getId() != $user->getId())
                unset($paniers[array_search($panier, $paniers)]);
        }
        return $paniers;
    }

    private function getCommandesForUser($user) {
        $commandes = $this->getDoctrine()->getRepository(Commande::class)->findAll();
        foreach ($commandes as $commande) {
            if ($commande->getUser()->getId() != $user->getId())
                unset($commandes[array_search($commande, $commandes)]);
        }
        return $commandes;
    }
}