<?php
namespace App\Controller\Client;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use DateTime;

use App\Entity\Produit;
use App\Entity\Panier;

class PanierController extends AbstractController
{
    /**
     * @Route("/client", name="client_panier", methods={"GET", "POST"})
     * @Route("/client/panier/show", name="client_panier_show", methods={"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showPanier(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $paniers = $this->getDoctrine()->getRepository(Panier::class)->findAll();
        /* Cleanup paniers for current user only. */
        $paniers = $this->cleanUserPaniers($user, $paniers);
        $produits = $this->getDoctrine()->getRepository(Produit::class)->findBy([], ['typeProduit' => 'ASC', 'stock' => 'ASC']);
        return $this->render('client/boutique/panier.twig', ['produits' => $produits, 'panier' => $paniers]);
    }

    /**
     * @Route("/client/panier/add", name="client_panier_add", methods={"POST"})
     * @param Request $request
     */
    public function addPanier(Request $request)
    {
        /* Common variables for increment and addition operations. */
        $manager = $this->getDoctrine()->getManager();
        /* Get needed data for panier manipulation. */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $produit = $this->getDoctrine()->getRepository(Produit::class)->find($_POST["id"]);
        $paniers = $this->cleanUserPaniers(
            $user,
            $this->getDoctrine()->getRepository(Panier::class)->findAll()
        );



        /* Increment if exists. */
        foreach ($paniers as $panier) {
            if ($panier->getProduit() == $produit) {
                if (isset($_POST["increment"]))
                    $this->incrementPanierCount($manager, $panier, $_POST["increment"]);
                else
                    $this->incrementPanierCount($manager, $panier, 1);
                return $this->redirectToRoute("client_panier_show");
            }
        }

        /* Add new panier. */
        $p = new Panier();
        $p->setUser($user);
        $p->setProduit($produit);
        $p->setQuantite(1);
        $p->setDateAchat(new DateTime("now"));
        /* Persist new panier. */
        $manager->persist($p);
        $manager->flush();
        $user->addPanier($p);
        /* Redirect on panier list. */
        return $this->redirectToRoute("client_panier_show");
    }

    /**
     * @Route("/client/panier/delete", name="client_panier_delete", methods={"POST"})
     * @param Request $request
     */
    public function deletePanier(Request $request) {
        $id = $request->request->get("id");
        return $this->deletePanierId($id);
    }

    public function deletePanierId(int $id) {
        $manager = $this->getDoctrine()->getManager();
        /* Find panier object. */
        $panier = $manager->getRepository(Panier::class)->find($id);
        $manager->remove($panier);
        $manager->flush();
        /* Show panier list. */
        return $this->redirectToRoute("client_panier_show");
    }

    private function incrementPanierCount($manager, Panier $panier, $inc = 1) {
        $panier->setQuantite($panier->getQuantite() + $inc);
        $manager->flush();
        if ($panier ->getQuantite() <= 0) {
            $this->deletePanierId($panier->getId());
        }
        return $this->redirectToRoute("client_panier_show");
    }

    public function getPaniersForUser($user) {
        return $this->cleanUserPaniers(
            $user,
            $this->getDoctrine()->getRepository(Panier::class)->findAll()
        );
    }

    private function cleanUserPaniers($user, $paniers) {
        foreach ($paniers as $panier) {
            if ($panier->getUser()->getId() != $user->getId())
                unset($paniers[array_search($panier, $paniers)]);
        }
        return $paniers;
    }
}