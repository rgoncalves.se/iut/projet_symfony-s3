#!/bin/sh

# php bin/console make:entity Salle
# php bin/console make:entity Ordinateur

php bin/console doctrine:database:create
php bin/console doctrine:schema:validate
php bin/console doctrine:schema:update  --force --dump-sql --complete
# php bin/console make:entity --regenerate

php bin/console doctrine:fixtures:load  --verbose --no-interaction

